module.exports = (sequelize, DataTypes) => {

    const Member = sequelize.define("member", {
        image: {
            type: DataTypes.STRING
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
      
        published: {
            type: DataTypes.BOOLEAN
        }
    
    })

    return Member

}