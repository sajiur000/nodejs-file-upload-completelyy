// import controllers review, products
const productController = require('../controllers/productController.js')
const reviewController = require('../controllers/reviewController')
const express = require('express'); 
const productModel = require('../models/productModel.js');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const db = require('../models')

// image Upload
const multer = require('multer')
const path = require('path')


// router
const router = require('express').Router()
const Product = db.products

router.get("/allProducts", (req, res) => {
    Product.findAll()
        .then((products) =>
     
            res.render("plist", {
                products,
            })
        )
        .catch((err) => res.render("error", { error: err }));

    // Display add gig form
});



router.get('/addProduct', (req, res) => {
  res.render('add', {layout: false});
})

// use routers
router.post('/addProduct', productController.upload , productController.addProduct)

// router.get('/allProducts', productController.getAllProducts)



router.get('/published', productController.getPublishedProduct)



// Review Url and Controller

router.get('/allReviews', reviewController.getAllReviews)
router.post('/addReview/:id', reviewController.addReview)

// get product Reviews
router.get('/getProductReviews/:id', productController.getProductReviews)




// Products router
router.get('/:id', productController.getOneProduct)

router.put('/:id', productController.updateProduct)

router.delete('/:id', productController.deleteProduct)

module.exports = router