// import controllers review, products
const teamController = require('../controllers/teamController.js')


// router
const router = require('express').Router()


// use routers
router.post('/addMember', teamController.upload , teamController.addMember)

router.get('/allMember', teamController.getAllMembers)

router.get('/published', teamController.getPublishedMember)





// Products router
router.get('/:id', teamController.getOneMember)

router.put('/:id', teamController.updateMember)

router.delete('/:id', teamController.deleteMember)

module.exports = router