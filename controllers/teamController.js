const db = require('../models')

// image Upload
const multer = require('multer')
const path = require('path')


// create main Model
const Team = db.members


// main work

// 1. create product

const addMember = async (req, res) => {

    let info = {
        image: req.file.path,
        title: req.body.title,
        published: req.body.published ? req.body.published : false
    }

    const member = await Member.create(info)
    res.status(200).send(member)
    console.log(member)

}



// 2. get all products

const getAllMembers = async (req, res) => {

    let members = await Member.findAll({})
    res.status(200).send(members)

}

// 3. get single product

const getOneMember = async (req, res) => {

    let id = req.params.id
    let member = await Member.findOne({ where: { id: id }})
    res.status(200).send(member)

}

// 4. update Product

const updateMember = async (req, res) => {

    let id = req.params.id

    const member = await Member.update(req.body, { where: { id: id }})

    res.status(200).send(member)
   

}

// 5. delete product by id

const deleteMember = async (req, res) => {

    let id = req.params.id
    
    await Member.destroy({ where: { id: id }} )

    res.status(200).send('member  is deleted !')

}

// 6. get published product

const getPublishedMember = async (req, res) => {

    const members =  await Member.findAll({ where: { published: true }})

    res.status(200).send(members)

}


// 8. Upload Image Controller

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'Images')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)  
        const extname = fileTypes.test(path.extname(file.originalname))

        if(mimeType && extname) {
            return cb(null, true)
        }
        cb('Give proper files formate to upload')
    }
}).single('image')









module.exports = {
    addMember,
    getAllMembers,
    getOneMember,
    updateMember,
    deleteMember,
    getPublishedMember,
    upload
    
}