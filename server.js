const express = require('express')
const bodyParser = require('body-parser');
const path = require('path')
const exphbs = require('express-handlebars');
const app = express()

// middleware

app.use(express.json())

app.use(express.urlencoded({ extended: true }))
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'hbs');

// routers
const router = require('./routes/productRouter.js')

// app.use('/api/products', router)
//static Images Folder

app.use('/Images', express.static('./Images'))
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/', require('./routes/home'));
app.use('/productRouter' , require('./routes/productRouter.js'));

//port

const PORT = process.env.PORT || 5000

//server

app.listen(PORT, () => {
    console.log(`server is running on port ${PORT}`)
})